package com.example.eighthhomework

interface FlowerItemListener {
    fun flowerItemOnClick(position: Int)

    fun flowerNextActivityClick(position: Int, name: String, description: String, photo: Int?)
}