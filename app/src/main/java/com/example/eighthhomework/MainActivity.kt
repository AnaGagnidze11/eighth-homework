package com.example.eighthhomework

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.eighthhomework.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val flowers = mutableListOf<FlowerModel>()

    private lateinit var adapter: RecyclerViewAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setTheme(R.style.Theme_EighthHomework)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        addFlowers()
        adapter = RecyclerViewAdapter(flowers, object : FlowerItemListener {
            override fun flowerItemOnClick(position: Int) {
                flowers.removeAt(position)
                adapter.notifyItemRemoved(position)
            }

            override fun flowerNextActivityClick(
                position: Int,
                name: String,
                description: String,
                photo: Int?
            ) {
                openActivity(name, description, photo)
            }
        })
        binding.recyclerView.layoutManager = GridLayoutManager(this, 2)
        binding.recyclerView.adapter = adapter

    }

    private fun openActivity(name: String, description: String, photo: Int?) {
        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("flowerName", name)
        intent.putExtra("flowerDesc", description)
        intent.putExtra("flowerPhoto", photo)
        startActivity(intent)
    }

    private fun addFlowers() {
        flowers.add(FlowerModel("Bluebell", "Symbolizes Gratitude", R.mipmap.ic_bluebell))
        flowers.add(FlowerModel("Daisy", "Symbolizes Purity", R.mipmap.ic_daisy))
        flowers.add(FlowerModel("Rose", "Symbolizes Romance"))
        flowers.add(FlowerModel("Freesia", "Symbolizes Trust", R.mipmap.ic_freesia))
        flowers.add(FlowerModel("Orchid", "Represents Fertility"))
        flowers.add(FlowerModel("Iris", "Represents Faith", R.mipmap.ic_iris))
        flowers.add(FlowerModel("Carnation", "Symbolizes Affection"))
        flowers.add(FlowerModel("Ixora", "Represents Passion", R.mipmap.ic_ixora))
        flowers.add(FlowerModel("Anemone", "Symbolizes Fragility"))
        flowers.add(FlowerModel("Lily", "Symbolizes Devotion", R.mipmap.ic_lily))
        flowers.add(FlowerModel("Poppy", "Symbolizes Peace", R.mipmap.ic_poppy))
        flowers.add(FlowerModel("Gladiolus", "Represents Strength"))
        flowers.add(FlowerModel("Nasturtium", "Represents Victory", R.mipmap.ic_nasturtium))
        flowers.add(FlowerModel("Pea", "Symbolizes Departure", R.mipmap.ic_pea))
        flowers.add(FlowerModel("Chrysanthemum", "Represents Happiness"))
        flowers.add(FlowerModel("Sunflower", "Represents Adoration", R.mipmap.ic_sunflower))
        flowers.add(FlowerModel("Tulip", "Symbolizes Deep Love", R.mipmap.ic_tulip))
    }
}