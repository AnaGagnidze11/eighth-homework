package com.example.eighthhomework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import com.example.eighthhomework.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        init()
    }

    private fun init() {
        val flowerName = intent.extras?.getString("flowerName", "")
        val flowerDesc = intent.extras?.getString("flowerDesc", "")
        val flowerPhoto = intent.extras?.getInt("flowerPhoto", R.mipmap.ic_flower)


        binding.TxVName.text = flowerName
        binding.TxVDesc.text = flowerDesc
        binding.ImgVPhoto.setImageResource(flowerPhoto ?: R.mipmap.ic_flower)
    }

}