package com.example.eighthhomework

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.eighthhomework.databinding.FlowerWithPicLayoutBinding
import com.example.eighthhomework.databinding.JustFlowerLayoutBinding


class RecyclerViewAdapter(
    private val flowers: MutableList<FlowerModel>,
    private val flowerItemListener: FlowerItemListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val JUST_FLOWER = 1
        private const val FLOWER_WITH_PIC = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == JUST_FLOWER)
            JustFlowerViewHolder(
                JustFlowerLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        else
            FlowerWithPicViewHolder(
                FlowerWithPicLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is FlowerWithPicViewHolder -> holder.bindInfo()
            is JustFlowerViewHolder -> holder.bindInfo()
        }
    }

    override fun getItemCount() = flowers.size

    inner class FlowerWithPicViewHolder(private val binding: FlowerWithPicLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnLongClickListener, View.OnClickListener {
        private lateinit var flowerItem: FlowerModel
        fun bindInfo() {
            flowerItem = flowers[adapterPosition]
            binding.TxVFlowerName.text = flowerItem.name
            binding.TxVFlowerDesc.text = flowerItem.desc
            binding.imPhoto.setImageResource(flowerItem.photo ?: R.mipmap.ic_flower)
            binding.root.setOnLongClickListener(this)
            binding.root.setOnClickListener(this)
        }

        override fun onLongClick(v: View?): Boolean {
            flowerItemListener.flowerItemOnClick(adapterPosition)
            return true
        }

        override fun onClick(v: View?) {
            flowerItemListener.flowerNextActivityClick(
                adapterPosition,
                flowerItem.name,
                flowerItem.desc,
                flowerItem.photo
            )
        }


    }

    inner class JustFlowerViewHolder(private val binding: JustFlowerLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnLongClickListener, View.OnClickListener {
        private lateinit var flowerItem: FlowerModel
        fun bindInfo() {
            flowerItem = flowers[adapterPosition]
            binding.TxVFlowerName.text = flowerItem.name
            binding.TxVFlowerDesc.text = flowerItem.desc
            binding.root.setOnLongClickListener(this)
            binding.root.setOnClickListener(this)
        }

        override fun onLongClick(v: View?): Boolean {
            flowerItemListener.flowerItemOnClick(adapterPosition)
            return true
        }

        override fun onClick(v: View?) {
            flowerItemListener.flowerNextActivityClick(
                adapterPosition,
                flowerItem.name,
                flowerItem.desc,
                flowerItem.photo
            )
        }

    }

    override fun getItemViewType(position: Int): Int {
        val model = flowers[position]
        return if (model.photo == null) JUST_FLOWER
        else FLOWER_WITH_PIC
    }
}