package com.example.eighthhomework

data class FlowerModel(val name: String, val desc: String, val photo: Int? = null)